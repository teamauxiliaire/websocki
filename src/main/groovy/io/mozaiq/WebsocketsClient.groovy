package io.mozaiq

import groovy.json.JsonOutput
import io.vertx.core.AbstractVerticle
import io.vertx.core.MultiMap
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.CaseInsensitiveHeaders
import io.vertx.core.http.WebSocket

class WebsocketsClient extends AbstractVerticle {

  final String id = 'websocki'

  WebSocket ws

  String state = ''

  @Override
  void start() {
    super.start()

    MultiMap hdrs = new CaseInsensitiveHeaders()
    hdrs.setAll gwId:'mygatewayid007', userId:'5ae0376fbfe5230001ae4cf1'

    Map opts = [ idleTimeout:30_000, ssl:true ]

    // Setting host as localhost is not strictly necessary as it's the default
    vertx.createHttpClient( opts ).websocket( 443, 'event-ws.testing.moqops.com', '', hdrs, { WebSocket websocket ->
      ws = websocket
      ws.handler{ Buffer data ->
        println("Received " + data)
        if( 4 == data.toString().size() ) return
        def json = data.toJsonObject()
        switch( json.command ) {
          case 'discovery':
            sendDiscovery()
            break
          default:
            state = json["${id}@@Text"]
            websocket.writeFinalTextFrame JsonOutput.toJson( data:[ [ id: "${id}@@Text", state:state, re:true ] ] )
        }
      }
      ws.closeHandler{ println "socket closed" }
      ws.exceptionHandler{ println it.cause() }
      println "socket created ${ws}"
      
      //Send some data
      sendDiscovery()
    }, { Throwable t ->
      t.printStackTrace()
    } )

    vertx.setPeriodic( 50_000 ){
      ws?.writeTextMessage 'ping'
    }
    // Simulating internal state changes:
    vertx.setPeriodic( 180_000 ){
      state = "Websocket: ok [${new Date()}]"
      ws?.writeTextMessage JsonOutput.toJson( data:[ [ id: "${id}@@Text", state:state ] ] )
    }
  }

  void sendDiscovery() {
    def dis = [ type: 'discoveryResponse', data: [[ id: id, title: 'Websocki', features:[[definition:'q:Data:System:Messenger m:Text', mode:'RW']] ] ] ]
    ws?.writeTextMessage JsonOutput.toJson( dis )
  }

}