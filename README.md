# websocki

## A concise example of implementing a websocket client to the direct gatway of the IoT platform mozaiq

The client registers a device for a direct websocket connection with the contract of supporting *Text* measures, receives and confirms updates, and sends updates on a regular basis automatically.

* Discovery is implemented starting with line 63.
* Receive is on line 40.
* Send (scheduled periodically) as of line 57.

Note: configuration data is not separated from the sources, and expected to be updated as necessary.

* line 23: gateway and end user id
* line 28: direct gateway endpoint